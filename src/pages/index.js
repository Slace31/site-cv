import ExpPage from "./ExpPage"
import InfoPage from "./InfoPage"
import FormPage from "./FormPage"
import CompPage from "./CompPage"
import RealPage from "./RealPage"

export {
    ExpPage,
    InfoPage,
    FormPage,
    CompPage,
    RealPage,
}